#!/bin/bash -ex

set -o pipefail

PYTHON=python3

export IFO

case $IFO in
    'H1')
        START=1388947100
        END=1388947200
        NEVENTS=1
        EVENT=1388947149
	;;
    'L1')
        START=1389001700
        END=1389001800
        NEVENTS=1
        EVENT=1389001751
        ;;
    *)
        echo "TEST FAIL: unknown IFO"
        exit 1
        ;;
esac

export LOG_LEVEL=DEBUG
export LOCKLOST_EVENT_ROOT=$(mktemp -d)
export LOCKLOST_WEB_ROOT=https://ldas-jobs.ligo.caltech.edu/~lockloss

# create local nds2 lightweight server, if one is not already
# available, since for some reason it has access to more data not on
# tape

trap "echo TEST FAIL: see $LOCKLOST_EVENT_ROOT" EXIT

$PYTHON -m locklost search "$START" "$END"

$PYTHON -m locklost compress

nsegs=$(ls -1 "${LOCKLOST_EVENT_ROOT}/.segments" | wc | awk '{print $1}')
test $nsegs -eq 1
# FIXME: use native formatting in gpstime 0.4
SSTART=$(printf %0.0f $(gpstime -g "$START"))
SEND=$(printf %0.0f $(gpstime -g "$END"))
test -e "${LOCKLOST_EVENT_ROOT}/.segments/${SSTART}-${SEND}"

$PYTHON -m locklost list

nevents=$($PYTHON -m locklost list | wc | awk '{print $1}')
test $nevents -eq $NEVENTS

$PYTHON -m locklost analyze $EVENT

$PYTHON -m locklost show $EVENT

$PYTHON -m locklost plot-history $LOCKLOST_EVENT_ROOT/history.svg

$PYTHON -m locklost summary --path $LOCKLOST_EVENT_ROOT/summary

REQUEST_METHOD=GET $PYTHON -m locklost.web >/dev/null

REQUEST_METHOD=GET QUERY_STRING=event=$EVENT $PYTHON -m locklost.web >/dev/null

REQUEST_METHOD=GET QUERY_STRING='format=json' $PYTHON -m locklost.web | tail -n +5 | json_verify
REQUEST_METHOD=GET QUERY_STRING='format=json&limit=' $PYTHON -m locklost.web | tail -n +5 | json_verify
REQUEST_METHOD=GET QUERY_STRING='format=json&limit=2' $PYTHON -m locklost.web | tail -n +5 | json_verify
REQUEST_METHOD=GET QUERY_STRING='format=json&tag=REFINED' $PYTHON -m locklost.web | tail -n +5 | json_verify

trap - EXIT

echo "TEST PASS"

rm -rf "$LOCKLOST_EVENT_ROOT"
