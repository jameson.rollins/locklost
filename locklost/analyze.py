import os
import sys
import logging
import argparse

from matplotlib import pyplot as plt

from . import __version__
from . import set_signal_handlers
from . import config_logger
from . import logger
from . import config
from .plugins import PLUGINS
from .event import LocklossEvent, find_events
from . import condor
from . import search


##################################################


def analyze_event(event, plugins=None):
    """Execute all follow-up plugins for event

    """
    # log analysis to event log
    # always append log
    log_mode = 'a'
    handler = logging.FileHandler(event.path('log'), mode=log_mode)
    handler.setFormatter(logging.Formatter(config.LOG_FMT))
    logger.addHandler(handler)

    # log exceptions
    def exception_logger(exc_type, exc_value, traceback):
        logger.error("Uncaught exception:", exc_info=(exc_type, exc_value, traceback))

    sys.excepthook = exception_logger

    try:
        event.lock()
    except OSError as e:
        logger.error(e)
        raise

    # clean up previous run
    if not plugins:
        event._scrub(archive=False)
    event._set_version(__version__)

    logger.info("analysis version: {}".format(event.analysis_version))
    logger.info("event id: {}".format(event.id))
    logger.info("event path: {}".format(event.path()))
    logger.info("event url: {}".format(event.url()))

    complete = True
    for name, func in PLUGINS.items():
        if plugins and name not in plugins:
            continue
        logger.info("executing plugin: {}({})".format(
            name, event.id))
        try:
            func(event)
        except SystemExit:
            complete = False
            logger.error("EXIT signal, cleaning up...")
            break
        except AssertionError:
            complete = False
            logger.exception("FATAL exception in {}:".format(name))
            break
        except Exception:
            complete = False
            logger.exception("exception in {}:".format(name))
        plt.close('all')

    if complete:
        logger.info("analysis complete")
        event._set_status(0)
    else:
        logger.warning("INCOMPLETE ANALYSIS")
        event._set_status(1)

    logger.removeHandler(handler)
    event.release()

    return complete


def analyze_condor(event):
    if event.analyzing:
        logger.warning("event analysis already in progress, aborting")
        return
    condor_dir = event.path('condor')
    try:
        os.makedirs(condor_dir)
    except Exception:
        pass
    sub = condor.CondorSubmit(
        condor_dir,
        'analyze',
        [str(event.id)],
        local=False,
        notify_user=os.getenv('CONDOR_NOTIFY_USER'),
    )
    sub.write()
    sub.submit()

##################################################


def _parser_add_arguments(parser):
    from .util import GPSTimeParseAction
    egroup = parser.add_mutually_exclusive_group(required=True)
    egroup.add_argument('event', nargs='?', type=int,
                        help="event ID / GPS second")
    egroup.add_argument('--condor', action=GPSTimeParseAction, nargs=2, metavar='GPS',
                        help="condor analyze all events within GPS range.  If the second time is '0' (zero), the first time will be interpreted as an exact event time and just that specific event will be analyzed via condor submit.")
    parser.add_argument('--rerun', action='store_true',
                        help="condor re-analyze events")
    parser.add_argument('--plugin', '-p', action='append',
                        help="execute only specified plugin (multiple ok, not available for condor runs)")


def main(args=None):
    """Analyze event(s)

    By default all analysis plugins for the specified event will
    exeuted.  If the --condor option is given with GPS start and end
    times all events within the specified time span will be analyzed.

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    if args.condor:
        t0, t1 = tuple(int(t) for t in args.condor)
        if t1 == 0:
            events = [LocklossEvent(t0)]
        else:
            logger.info("finding events...")
            events = [e for e in find_events(after=t0, before=t1)]
        to_analyze = []
        for e in events:
            if e.analyzing:
                logger.debug("  {} analyzing".format(e))
                continue
            if e.analysis_succeeded and not args.rerun:
                logger.debug("  {} suceeded".format(e))
                continue
            logger.debug("  adding {}".format(e))
            to_analyze.append(e)
        logger.info("found {} events, {} to analyze.".format(len(events), len(to_analyze)))
        if not to_analyze:
            return

        def condor_args_gen():
            for e in to_analyze:
                yield [('event', e)]

        dag = condor.CondorDAG(
            config.CONDOR_ANALYZE_DIR,
            'analyze',
            condor_args_gen,
            log='logs/$(event).',
        )
        dag.write()
        dag.submit()

    else:
        event = None
        try:
            event = LocklossEvent(args.event)
        except OSError:
            pass
        if not event:
            logger.info("no event matching GPS {}.  searching...".format(int(args.event)))
            search.search((args.event-1, args.event+1))
            try:
                event = LocklossEvent(args.event)
            except OSError as e:
                sys.exit(e)
        logger.info("analyzing event {}...".format(event))
        if not analyze_event(event, args.plugin):
            sys.exit(1)


# direct execution of this module intended for condor jobs
if __name__ == '__main__':
    set_signal_handlers()
    config_logger(level='DEBUG')
    parser = argparse.ArgumentParser()
    parser.add_argument('event', type=int,
                        help="event ID / GPS second")
    args = parser.parse_args()
    event = LocklossEvent(args.event)
    if not analyze_event(event):
        sys.exit(1)
