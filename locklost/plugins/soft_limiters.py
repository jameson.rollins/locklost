import numpy as np

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data


##############################################


def check_soft_limiters(event):

    """Checks if ASC limits are reached for Pitch and Yaw and creates a tag if railed.

    """

    if config.IFO == 'L1':
        logger.info("followup is not configured or not applicable to LLO.")
        return

    mod_window = [config.SOFT_SEARCH_WINDOW[0], config.SOFT_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    data_collect = data.fetch(config.ASC, segment, as_dict=True)
    # collects all ASC data for analysis

    soft_limits = False

    ASC_INMON = [val.data for key, val in data_collect.items() if key in config.ASC_INMON]
    # ASC raw input signal
    ASC_LIMIT = [val.data for key, val in data_collect.items() if key in config.ASC_LIMIT]
    # ASC soft loop limits
    ASC_ENABLE = [val.data for key, val in data_collect.items() if key in config.ASC_ENABLE]
    # ASC soft loop switch

    inputs = [max(i) for i in ASC_INMON]
    # pull inmon channels into a list

    inputs_max = [max(i) for i in ASC_LIMIT]
    # pull threshold values

    switch = [max(i) for i in ASC_ENABLE]
    # pull toggle values

    finallist = []

    abs_set_diff = np.abs(inputs)
    # take absolute value

    sub = np.subtract(abs_set_diff, inputs_max)
    # subtract array from limits, if array > 0 it is railed

    sub = sub.tolist()
    finallist.append(sub)

    for el, s in zip(finallist[0], switch):
        if el > 0 and s == 1:
            soft_limits = True
            logger.info('{} is railed.'.format(config.ASC_INMON[finallist[0].index(el)]))
            break

    if soft_limits:
        event.add_tag('SOFT_LIMITERS')
    else:
        logger.info('No ASC channel hit its soft limit.')
