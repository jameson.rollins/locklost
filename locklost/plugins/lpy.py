import os
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data
from .. import plotutils


#################################################


def find_lpy(event):
    """Create length, pitch, and yaw plots

    For first saturated channel centered around the lock loss time.

    """

    plotutils.set_rcparams()
    gps = event.gps

    # determine first suspension channel to saturate from saturation plugin
    infile_csv = 'saturations.csv'
    inpath_csv = event.path(infile_csv)
    sat_channel = ''

    if not os.path.exists(inpath_csv):
        logger.info('No saturating suspensions, plotting ETMX L3 LPY')
        sat_channel = f'{config.IFO}:SUS-ETMX_L3_MASTER_OUT_UR_DQ'
    else:
        with open(inpath_csv, 'r') as f:
            first_sat = f.readline()
            if first_sat:
                sat_channel, sat_time = first_sat.split(' ', 1)

    # generate the LPY mapping and channels to query from base channel
    lpy_map = channel2lpy_coeffs(sat_channel)
    base_channel, _, _ = sat_channel.rsplit('_', 2)
    channels = [get_sus_channel(base_channel, corner) for corner in lpy_map['corners']]

    for window_type, window in config.PLOT_WINDOWS.items():
        logger.info('Making {} LPY plot for {}'.format(window_type, base_channel))

        segment = Segment(window).shift(gps)
        bufs = data.fetch(channels, segment)

        srate = bufs[0].sample_rate
        t = np.arange(window[0], window[1], 1/srate)

        # calculate LPY using mapping from channels to coeffs
        titles = ['length', 'pitch', 'yaw']
        lpy = defaultdict(lambda: np.zeros(len(bufs[0].data)))
        for title in titles:
            for corner, coeff in lpy_map[title].items():
                idx = channels.index(get_sus_channel(base_channel, corner))
                # If ETMX, make threshold < saturation value so we can see better
                if base_channel[0:11] == 'H1:SUS-ETMX':
                    threshold = 524288
                    lpy[title] += (bufs[idx].data * coeff) / (4 * threshold)
                else:
                    threshold = config.get_saturation_threshold(get_sus_channel(base_channel, corner), gps, config.IFO)
                    lpy[title] += (bufs[idx].data * coeff) / (4 * threshold)

        colors = ['#2c7fb8', '#e66101', '#5e3c99']
        fig, axs = plt.subplots(3, sharex=True, figsize=(27, 16))
        for ax, dof, color, title in zip(axs, lpy.values(), colors, titles):
            ax.plot(
                t,
                dof,
                color=color,
                label=title,
                alpha=0.8,
                lw=2,
            )
            if base_channel[0:11] == 'H1:SUS-ETMX':
                label = '524288 cts (sat threshold = 134217728)'
            else:
                label = 'saturation threshold'
            ax.axhline(
                1,
                color='red',
                linestyle='--',
                lw=3,
                label=label,
            )
            ax.axhline(
                -1,
                color='red',
                linestyle='--',
                lw=3,
            )

            ax.set_ylim(-1.2, 1.2)
            ax.set_xlim(window[0], window[1])
            ax.set_xlabel(
                'Time [s] since lock loss at {}'.format(gps),
                labelpad=10,
            )
            ax.grid()
            ax.legend()

        fig.text(0.04, 0.5, r"Counts", ha='center', va='center', rotation='vertical')
        fig.subplots_adjust(hspace=0)
        plt.setp([ax.get_xticklabels() for ax in fig.axes[:-1]], visible=False)

        # set title
        axs[0].set_title("Length-Pitch-Yaw Plots for {}".format(base_channel))

        outfile_plot = 'lpy_{}.png'.format(window_type)
        outpath_plot = event.path(outfile_plot)
        fig.savefig(outpath_plot, bbox_inches='tight')
        fig.clf()


def get_sus_channel(base_channel, corner):
    return '{}_{}_DQ'.format(base_channel, corner)


def extract_sus_channel(channel):
    """Extract optic, stage, corner info out of a SUS channel name.

    """
    prefix, name = channel.split('-', 1)
    optic, stage, _, _, corner, _ = name.split('_', 5)
    return optic, stage, corner


def channel2lpy_coeffs(channel):
    """From a suspension channel, get a dictionary containing the LPY coefficients.

    """
    lpy_map = {}
    optic, stage, corner = extract_sus_channel(channel)

    if optic in ['ETMX', 'ETMY', 'ITMX', 'ITMY']:
        if stage in ['M0']:
            F2_gain = -1
            F3_gain = 1
            lpy_map['length'] = {'F2': -1 * F2_gain, 'F3': -1 * F3_gain}
            lpy_map['pitch'] = {'F2': 1 * F2_gain, 'F3': 1 * F3_gain}
            lpy_map['yaw'] = {'F2': 1 * F2_gain, 'F3': -1 * F3_gain}
            lpy_map['corners'] = ['F2', 'F3']
        elif stage in ['L1']:
            if config.IFO == 'H1':
                if optic in ['ETMX']:
                    UL_gain = -1
                    LL_gain = 1
                    UR_gain = -1
                    LR_gain = 1
                else:
                    UL_gain = -1
                    LL_gain = 1
                    UR_gain = 1
                    LR_gain = -1
            else:
                UL_gain = -1
                LL_gain = 1
                UR_gain = 1
                LR_gain = -1
            lpy_map['length'] = {'UL': 1 * UL_gain, 'LL': 1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['pitch'] = {'UL': 1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': -1 * LR_gain}
            lpy_map['yaw'] = {'UL': -1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
        elif stage in ['L2']:
            if config.IFO == 'H1':
                UL_gain = 1
                LL_gain = -1
                UR_gain = -1
                LR_gain = 1
            else:
                UL_gain = -1
                LL_gain = 1
                UR_gain = 1
                LR_gain = -1
            lpy_map['length'] = {'UL': 1 * UL_gain, 'LL': 1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['pitch'] = {'UL': 1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': -1 * LR_gain}
            lpy_map['yaw'] = {'UL': -1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
        elif stage in ['L3']:
            UL_gain = 1
            LL_gain = 1
            UR_gain = 1
            LR_gain = 1
            lpy_map['length'] = {'UL': 1 * UL_gain, 'LL': 1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['pitch'] = {'UL': 1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': -1 * LR_gain}
            lpy_map['yaw'] = {'UL': -1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['MC1', 'MC2', 'MC3', 'PRM', 'PR2', 'PR3', 'SRM', 'SR2', 'SR3']:
        if stage in ['M1']:
            LF_gain = 1
            RT_gain = -1
            T2_gain = -1
            T3_gain = 1
            lpy_map['length'] = {'LF': 1 * LF_gain, 'RT': 1 * RT_gain}
            lpy_map['pitch'] = {'T2': 1 * T2_gain, 'T3': -1 * T3_gain}
            lpy_map['yaw'] = {'LF': -1 * LF_gain, 'RT': 1 * RT_gain}
            lpy_map['corners'] = ['T2', 'T3', 'LF', 'RT']
        elif stage in ['M2', 'M3']:
            if config.IFO == 'L1':
                UL_gain = -1
                LL_gain = 1
                UR_gain = 1
                LR_gain = -1
            if config.IFO == 'H1':
                UL_gain = 1
                LL_gain = -1
                UR_gain = -1
                LR_gain = 1
            lpy_map['length'] = {'UL': 1 * UL_gain, 'LL': 1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['pitch'] = {'UL': 1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': -1 * LR_gain}
            lpy_map['yaw'] = {'UL': -1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['BS']:
        if stage in ['M1']:
            LF_gain = 1
            RT_gain = -1
            F2_gain = 1
            F3_gain = -1
            lpy_map['length'] = {'LF': 1 * LF_gain, 'RT': 1 * RT_gain}
            lpy_map['pitch'] = {'F2': -1 * F2_gain, 'F3': -1 * F3_gain}
            lpy_map['yaw'] = {'LF': -1 * LF_gain, 'RT': 1 * RT_gain}
            lpy_map['corners'] = ['F2', 'F3']
        elif stage in ['M2']:
            if config.IFO == 'L1':
                UL_gain = -1
                LL_gain = 1
                UR_gain = 1
                LR_gain = -1
            if config.IFO == 'H1':
                UL_gain = 1
                LL_gain = -1
                UR_gain = -1
                LR_gain = 1
            lpy_map['length'] = {'UL': 1 * UL_gain, 'LL': 1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['pitch'] = {'UL': 1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': -1 * LR_gain}
            lpy_map['yaw'] = {'UL': -1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['OMC']:
        if stage in ['M1']:
            LF_gain = 1
            RT_gain = -1
            T2_gain = -1
            T3_gain = 1
            lpy_map['length'] = {'LF': 1 * LF_gain, 'RT': 1 * RT_gain}
            lpy_map['pitch'] = {'T2': 1 * T2_gain, 'T3': -1 * T3_gain}
            lpy_map['yaw'] = {'LF': -1 * LF_gain, 'RT': 1 * RT_gain}
            lpy_map['corners'] = ['T2', 'T3', 'LF', 'RT']
    elif optic in ['OM1', 'OM3', 'RM1']:
        if stage in ['M1']:
            UL_gain = 1
            LL_gain = -1
            UR_gain = -1
            LR_gain = 1
            lpy_map['length'] = {'UL': 1 * UL_gain, 'LL': 1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['pitch'] = {'UL': 1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': -1 * LR_gain}
            lpy_map['yaw'] = {'UL': -1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['RM2']:
        if stage in ['M1']:
            if config.IFO == 'L1':
                UL_gain = 1
                LL_gain = -1
                UR_gain = -1
                LR_gain = 1
            if config.IFO == 'H1':
                UL_gain = -1
                LL_gain = 1
                UR_gain = 1
                LR_gain = -1
            lpy_map['length'] = {'UL': 1 * UL_gain, 'LL': 1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['pitch'] = {'UL': 1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': -1 * LR_gain}
            lpy_map['yaw'] = {'UL': -1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['OM2', 'ZM1', 'ZM2']:
        if stage in ['M1']:
            UL_gain = -1
            LL_gain = 1
            UR_gain = 1
            LR_gain = -1
            lpy_map['length'] = {'UL': 1 * UL_gain, 'LL': 1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['pitch'] = {'UL': 1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': -1 * LR_gain}
            lpy_map['yaw'] = {'UL': -1 * UL_gain, 'LL': -1 * LL_gain, 'UR': 1 * UR_gain, 'LR': 1 * LR_gain}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    else:
        raise RuntimeError("no LPY map for suspension '{}'.".format(channel))

    return lpy_map
