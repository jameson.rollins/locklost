import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data
from .. import plotutils


##############################################


def check_wind(event):
    """Checks for elevated windspeed.

    Checks windspeed at corner and end stations, plots the data, and
    creates tag if above a certain threshold.

    """
    plotutils.set_rcparams()

    mod_window = [config.WIND_SEARCH_WINDOW[0], config.WIND_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    wind_channels = data.fetch(config.WIND_CHANNELS, segment)

    windy = False
    max_windspeed = 0
    thresh_crossing = segment[1]
    for buf in wind_channels:
        max_windspeed = max([max_windspeed, max(buf.data)])
        if any(buf.data > config.WIND_THRESH):
            windy = True
            srate = buf.sample_rate
            t = np.arange(segment[0], segment[1], 1/srate)
            glitch_idx = np.where(buf.data > config.WIND_THRESH)[0][0]
            glitch_time = t[glitch_idx]
            thresh_crossing = min(glitch_time, thresh_crossing)

    if windy:
        event.add_tag('WINDY')
    else:
        logger.info("wind speed below threshold")

    fig, ax = plt.subplots(1, figsize=(22, 16))
    for buf in wind_channels:
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        ax.plot(
            t-event.gps,
            buf.data,
            label=buf.channel,
            alpha=0.8,
            lw=2,
        )
    ax.axhline(
        config.WIND_THRESH,
        linestyle='--',
        color='black',
        label='windspeed threshold',
        lw=5,
    )
    if thresh_crossing != segment[1]:
        plotutils.set_thresh_crossing(ax, thresh_crossing, event.gps, segment)

    ax.grid()
    ax.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax.set_ylabel('Velocity [mph]')
    # ax.set_ylim(0, max_windspeed+1)
    ax.set_xlim(t[0]-event.gps, t[-1]-event.gps)
    ax.legend(loc='best')
    ax.set_title('End/corner station wind speeds', y=1.04)

    fig.tight_layout(pad=0.05)
    outfile_plot = 'windy.png'
    outpath_plot = event.path(outfile_plot)
    fig.savefig(outpath_plot, bbox_inches='tight')
    fig.clf()
