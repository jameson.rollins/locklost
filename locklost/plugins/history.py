import os

import numpy as np

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data


INITIAL_WINDOW = [-60, 0]


##################################################


def find_previous_state(event):
    """Collect info about previous guardian state

    Writes out a 'previous_state' file with format:

    previous_state start_gps end_gps lockloss_state

    """
    channels = [config.GRD_STATE_N_CHANNEL]

    # find the transition time
    segment = Segment(0, 1).shift(event.id)
    gbuf = data.fetch(channels, segment)[0]
    lli = np.where(gbuf.data == event.transition_index[1])[0]
    assert len(lli) > 0, "Lock loss not found at this time!"
    state_end_gps = gbuf.tarray[lli[0]]
    # note the transition time for old events
    state_end_file = event.path('guard_state_end_gps')
    if not os.path.exists(state_end_file):
        with open(state_end_file, 'w') as f:
            f.write('{:f}\n'.format(state_end_gps))

    # find start of previous state
    lockloss_found = False
    check_3_101_2 = 0
    check_3_2 = 0
    state_start_gps = None
    power = 1
    window = [INITIAL_WINDOW[0], 1]

    while not state_start_gps:
        # define search window and search for change in guardian state
        segment = Segment(*window).shift(state_end_gps)
        gbuf = data.fetch(channels, segment)[0]
        transitions = list(data.gen_transitions(gbuf))

        # check the transitions
        for transition in reversed(transitions):
            transt = transition[0]
            transi = tuple(map(int, transition[1:]))
            if lockloss_found:
                if config.IFO == 'H1' and transi[0] >= 600:
                    logger.info(
                        "Previous state was still in NLN, continue searching "
                        "for transition from state less than 600."
                    )
                    continue
                previous_index = event.transition_index[0]
                # Ignore matching up transi[1] with event.transition_index[0] if
                # state at lockloss was at or above 600
                if not (config.IFO == 'H1' and previous_index >= 600):
                    assert transi[1] == previous_index, \
                        "Transition history does not match lock loss " \
                        f"transition ({transi[1]} != {previous_index})"
                state_start_gps = transt
                break

            # For H1, a X->3->101->2 transition is a common pattern
            # that shows up as 101->2 but we actually want to record
            # them as X->3.  The following logic checks to see if
            # we're in one of these situations and updates the
            # transition_index information accordingly.
            #
            # and event.transition_index == (101, 2)
            if config.IFO == 'H1':
                if transi == (101, 2):
                    # could be this case, so mark as candidate and
                    # continue
                    check_3_101_2 = 1
                    continue
                elif check_3_101_2 == 1:
                    if transi == (3, 101):
                        # yup, this is it, note and continue
                        check_3_101_2 = 2
                        continue
                    else:
                        # nope, this is a regular 101->2 transtion, so
                        # note state 101 start time and break
                        lockloss_found = True
                        state_start_gps = transt
                        break
                elif check_3_101_2 == 2:
                    logger.info(
                        f'Updating H1 lockloss for X->3->101->2 '
                        f'transition: {transi}'
                    )
                    event.add_tag("FAST_DRMI")
                    # update the transition_index information on disk
                    # and clear cache so it's reloaded next access
                    with open(event.path('transition_index'), 'w') as f:
                        f.write('{} {}\n'.format(*transi))
                    event._transition_index = None
                    # don't continue so that we hit the lockloss_found
                    # check below

                # If we don't have a X->3->101->2 LL, check if we maybe have
                # a X->3->2 LL
                if transi == (3, 2):
                    # Mark as candidate for this type of LL
                    check_3_2 = 1
                    continue
                elif check_3_2 == 1:
                    logger.info(
                        f'Updating H1 lockloss for X->3->2 '
                        f'transition: {transi}'
                    )
                    event.add_tag("FAST_DRMI")
                    # update the transition_index information on disk
                    # and clear cache so it's reloaded next access
                    with open(event.path('transition_index'), 'w') as f:
                        f.write('{} {}\n'.format(*transi))
                    event._transition_index = None
                    # don't continue so that we hit the lockloss_found

            if transi == event.transition_index:
                logger.info(f'Lockloss found: {transi}')
                lockloss_found = True

        assert lockloss_found, "Lock loss not found in first segment"

        window = [edge * (2 ** power) + window[0] for edge in INITIAL_WINDOW]
        power += 1

    # write start of lock stretch to disk
    previous_index, lockloss_index = event.transition_index
    previous_state = (previous_index, state_start_gps, state_end_gps, lockloss_index)
    logger.info("Previous guardian state: {}".format(previous_state))
    with open(event.path('previous_state'), 'w') as f:
        f.write('{} {:f} {:f} {}\n'.format(*previous_state))
